const {Sequelize} = require('sequelize');
// const sequelize = new Sequelize({
//     dialect: 'sqlite',
//     storage: ':memory:',
//     logging: false,
//     define: {
//         timestamps: false
//     },
// });

// module.exports = sequelize;
module.exports = new Sequelize({
    dialect: 'sqlite',
    storage: ':memory:',
    logging: false,
    define: {
        timestamps: false
    },
});