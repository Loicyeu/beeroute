module.exports = {
    HOST: "localhost", //Exemple : 'localhost'
    PORT: 3000, //Exemple : '3000'
    BASE_URL: "/api/v1/" //Doit finir par un '/'
};