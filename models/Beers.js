"use strict";
const {Sequelize} = require("sequelize");

/**
 * Classe Bière.<br>
 * Contrôleur permettant de faire les appels à la base de données.
 */
class Beers {

    static BeerModel;

    //region GET
    /**
     * Méthode permettant de récupérer toutes les bières en fonctions des paramètres.
     * @param params {Object} La liste des paramètres de la recherche
     * @param   params.name {String} Le nom de la bière
     * @param   params.alcohol {Number} Le taux d'alcool de la bière
     * @param   params.breweryId {Number} L'id de la brasserie
     *
     * @return {Promise<Array<Object>|Object>} Les bières trouvées ou un message d'erreur.
     */
    static get(params) {
        return new Promise(async (resolve, reject) => {
            const beers = await this.BeerModel.findAll({
                where: this.cleanParamsMultiple(params)
            });

            if (beers.length === 0) {
                reject({error: "Aucune bière n'a été trouvé."});
            } else {
                resolve(this.cleanBeers(beers))
            }
        });
    }

    /**
     * Méthode permettant de récupérer une bière en fonction de son id
     * @param id L'id de la bière.
     * @return {Promise<Object>} La brasserie ou une erreur.
     */
    static getById(id) {
        return new Promise((resolve, reject) => {
            return this.BeerModel.findByPk(id)
                .then(beer => {
                    if (beer === null) {
                        reject({error: "La bière n'a pas pu être trouvé."});
                    } else {
                        resolve(this.cleanBeer(beer));
                    }
                })
                .catch(() => reject({error: "La bière n'a pas pu être trouvé."}));
        });
    }

    /**
     * Méthode permettant de récupérer un nombre de bières aléatoires.
     * @param number {Number} Le nombre de bières à récupérer.
     * @return {Promise<Object>} La bière aléatoire ou un message d'erreur.
     */
    static getRandom(number) {
        return new Promise((resolve, reject) => {
            return this.BeerModel.findAll({order: Sequelize.literal('RANDOM()'), limit: number})
                .then(beers => resolve(this.cleanBeers(beers)))
                .catch(() => reject({error: "Aucune bière n'a été trouvé."}));
        });
    }

    //endregion

    //region ADD
    /**
     * Méthode permettant d'ajouter une bière à la base de donnée.
     * @param beer {Object} La bière a ajouter.
     * @param beer.id {Number} L'id de la bière.
     * @param beer.name {String} Le nom de la bière.
     * @param beer.alcohol {Number} Le taux d'alcool de la bière.
     * @param beer.breweryId {Number} L'id de la brasserie de la bière.
     * @return {Promise<Object>} La bière ajouté ou un message d'erreur.
     */
    static add(beer) {
        return new Promise((resolve, reject) => {
            this.BeerModel.create(beer)
                .then(() => resolve(beer))
                .catch(reason => reject({error: "La bière existe déjà ou ne peut être ajouté."}));
        });
    }

    //endregion

    //region EDIT
    /**
     * Méthode permettant de modifier une bière dans la base de données.
     * @param params {Object} Les champs à modifier dans la bière.
     * @param params.name {String} Le nouveau nom de la bière.
     * @param params.alcohol {Number} Le nouveau taux d'alcool de la bière.
     * @param params.breweryId {Number} Le nouvel id de la brasserie de la bière.
     * @return {Promise<Object>} La bière modifié ou un message d'erreur.
     */
    static edit(params) {
        return new Promise((resolve, reject) => {
            return this.BeerModel.findByPk(params.id)
                .then(async beer => {
                    if (beer === null) {
                        return reject({error: "La bière n'a pas pu être modifiée."});
                    } else {
                        Object.assign(beer, this.cleanParams(params));
                        await beer.save();
                        return resolve(this.cleanBeer(beer));
                    }
                })
                .catch(() => reject({error: "La bière n'a pas pu être modifiée."}));
        });
    }

    //endregion

    //region DELETE
    /**
     * Méthode permettant de supprimer une bière de la base de données.
     * @param id L'id de la bière à supprimer.
     * @return {Promise<Object>} La bière supprimé ou un message d'erreur.
     */
    static delete(id) {
        return new Promise(async (resolve, reject) => {
            const beer = await this.BeerModel.findByPk(id);
            if (beer === null) {
                reject({error: "La bière n'a pas pu être supprimée."});
            } else {
                await beer.destroy();
                resolve(this.cleanBeer(beer));
            }
        });
    }

    //endregion

    //region UTILS
    /**
     * Méthode permettant de modifier l'Array pour le rendre compatible avec les modèles de JOI.
     * @param beers {Array}
     * @param beers.dataValues {Object}
     * @return {Array} L'Array nettoyé.
     */
    static cleanBeers(beers) {
        return beers.map(beer => this.cleanBeer(beer));
    }

    /**
     * Méthode permettant de modifier l'Object pour le rendre compatible avec les modèles de JOI.
     * @param beer {Object}
     * @param beer.dataValues {Object}
     * @return {Object} L'Object nettoyé
     */
    static cleanBeer(beer) {
        return beer.dataValues;
    }

    /**
     * Méthode permettant de nettoyer les params de tous les attributs superflus.
     * @param params Les paramètres.
     * @return {Object} Les paramètres nettoyés.
     */
    static cleanParamsMultiple(params) {
        const {name, alcohol, breweryId} = params;
        const ret = {};
        if (name !== void 0) {
            ret.name = name.split(',');
        }
        if (alcohol !== void 0) {
            ret.alcohol = alcohol.split(',');
        }
        if (breweryId !== void 0) {
            ret.breweryId = breweryId;
        }
        return ret;
    }

    /**
     * Méthode permettant de nettoyer les params de tous les attributs superflus.
     * @param params Les paramètres.
     * @return {Object} Les paramètres nettoyés.
     */
    static cleanParams(params) {
        const {name, alcohol, breweryId} = params;
        const ret = {};
        if (name !== void 0) {
            ret.name = name;
        }
        if (alcohol !== void 0) {
            ret.alcohol = alcohol;
        }
        if (breweryId !== void 0) {
            ret.breweryId = breweryId;
        }
        return ret;
    }

    //endregion
}

module.exports = Beers;