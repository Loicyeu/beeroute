"use strict";
const {Sequelize} = require("sequelize");

/**
 * Classe Brasserie.<br>
 * Contrôleur permettant de faire les appels à la base de données.
 */
class Breweries {

    static BreweriesModel;

    //region GET
    /**
     * Méthode permettant de récupérer toutes les brasseries en fonctions des paramètres.
     * @param params {Object} La liste des paramètres de la recherche
     * @param   params.name {String} Le nom de la brasserie
     * @param   params.city {String} La ville de la brasserie
     *
     * @return {Promise<Array<Object>|Object>} Les brasseries trouvées ou un message d'erreur.
     */
    static get(params) {
        return new Promise(async (resolve, reject) => {
            const breweries = await this.BreweriesModel.findAll({
                where: this.cleanParamsMultiple(params)
            });

            if (breweries.length === 0) {
                reject({error: "Aucune brasserie n'a été trouvé."});
            } else {
                resolve(this.cleanBreweries(breweries))
            }
        });
    }

    /**
     * Méthode permettant de récupérer une brasserie en fonction de son id
     * @param id L'id de la brasserie.
     * @return {Promise<Object>} La brasserie ou une erreur.
     */
    static getById(id) {
        return new Promise((resolve, reject) => {
            return this.BreweriesModel.findByPk(id)
                .then(brewery => {
                    if (brewery === null) {
                        reject({error: "La brasserie n'a pas pu être trouvé."});
                    } else {
                        resolve(this.cleanBrewery(brewery));
                    }
                })
                .catch(() => reject({error: "La brasserie n'a pas pu être trouvé."}));
        });
    }

    /**
     * Méthode permettant de récupérer un nombre de brasseries aléatoires.
     * @param number {Number} Le nombre de brasseries à récupérer.
     * @return {Promise<Object>} La brasserie aléatoire ou un message d'erreur.
     */
    static getRandom(number) {
        return new Promise((resolve, reject) => {
            return this.BreweriesModel.findAll({order: Sequelize.literal('RANDOM()'), limit: number})
                .then(beers => resolve(this.cleanBreweries(beers)))
                .catch(() => reject({error: "Aucune brasserie n'a été trouvé."}));
        });
    }

    //endregion

    //region ADD
    /**
     * Méthode permettant d'ajouter une brasserie à la base de donnée.
     * @param brewery {Object} La brasserie a ajouter.
     * @param   brewery.id {Number} L'id de la brasserie.
     * @param   brewery.name {String} Le nom de la brasserie.
     * @param   brewery.city {String} La ville de la brasserie.
     * @return {Promise<unknown>}
     */
    static add(brewery) {
        return new Promise((resolve, reject) => {
            this.BreweriesModel.create(brewery)
                .then(() => resolve(brewery))
                .catch(() => reject({error: "La brasserie existe déjà."}));
        });
    }

    //endregion

    //region EDIT
    /**
     * Méthode permettant de modifier une brasserie dans la base de données.
     * @param params {Object} Les champs à modifier dans la brasserie.
     * @param params.name {String} Le nouveau nom de la brasserie.
     * @param params.city {Number} La nouvelle ville de la brasserie.
     * @return {Promise<Object>} La brasserie modifié ou un message d'erreur.
     */
    static edit(params) {
        return new Promise(async (resolve, reject) => {
            const brewery = await this.BreweriesModel.findByPk(params.id);
            if (brewery === null) {
                return reject({error: "La brasserie n'a pas pu être modifiée."});
            } else {
                Object.assign(brewery, this.cleanParams(params));
                await brewery.save();
                return resolve(this.cleanBrewery(brewery));
            }
        });
    }

    //endregion

    //region DELETE
    /**
     * Méthode permettant de supprimer une brasserie de la base de données.
     * @param id {Number} L'id de la brasserie à supprimer.
     * @param force {Boolean} Vrai s'il faut forcer la suppression et supprimer les bières, faux sinon.
     * @return {Promise<Object>} La brasserie supprimé ou un message d'erreur.
     */
    static delete(id, force) {
        return new Promise(async (resolve, reject) => {
            const brewery = await this.BreweriesModel.findByPk(id);
            if (brewery === null) {
                reject({error: "La brasserie n'a pas pu être supprimée."});
            } else {
                const beers = await brewery.getBeers();
                if (beers.length > 0) {
                    if (force) {
                        for (const beer of beers) {
                            beer.destroy();
                        }
                    } else {
                        reject({error: "La brasserie n'a pas pu être supprimée."});
                        return
                    }
                }

                await brewery.destroy();
                resolve(this.cleanBrewery(brewery));
            }
        });
    }

    //endregion

    //region UTILS
    /**
     * Méthode permettant de modifier l'Array pour le rendre compatible avec les modèles de JOI.
     * @param breweries {Array}
     * @param breweries.dataValues {Object}
     * @return {Array} L'Array nettoyé.
     */
    static cleanBreweries(breweries) {
        return breweries.map(brew => this.cleanBrewery(brew));
    }

    /**
     * Méthode permettant de modifier l'Object pour le rendre compatible avec les modèles de JOI.
     * @param brewery {Object}
     * @param brewery.dataValues {Object}
     * @return {Object} L'Object nettoyé
     */
    static cleanBrewery(brewery) {
        return brewery.dataValues;
    }

    /**
     * Méthode permettant de nettoyer les params de tous les attributs superflus.
     * @param params Les paramètres.
     * @return {Object} Les paramètres nettoyés.
     */
    static cleanParamsMultiple(params) {
        const {name, city} = params;
        const ret = {};
        if (name !== void 0) {
            ret.name = name.split(",");
        }
        if (city !== void 0) {
            ret.city = city.split(",");
        }
        return ret;
    }

    /**
     * Méthode permettant de nettoyer les params de tous les attributs superflus.
     * @param params Les paramètres.
     * @return {Object} Les paramètres nettoyés.
     */
    static cleanParams(params) {
        const {name, city} = params;
        const ret = {};
        if (name !== void 0) {
            ret.name = name;
        }
        if (city !== void 0) {
            ret.city = city;
        }
        return ret;
    }

    //endregion
}

module.exports = Breweries;