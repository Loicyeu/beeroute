"use strict";
const Joi = require('joi');


/**
 * Modèle JOI représentant une brasserie
 * @type {Joi.ObjectSchema<any>}
 */
const breweryModel = Joi.object({
    id: Joi.number().integer().positive().description("L'identifiant unique de la brasserie").example(1),
    name: Joi.string().allow("").description("Le nom de la brasserie").example("Brasserie 1"),
    city: Joi.string().allow("").description("La ville de la brasserie").example("Nantes"),
}).label("Brasserie");

/**
 * Modèle JOI représentant un tableau de brasseries.
 * @type {Joi.ArraySchema}
 */
const breweriesModel = Joi.array().items(breweryModel.optional()).label("Brasseries");

/**
 * Modèle JOI représentant une erreur.
 * @type {Joi.ObjectSchema<any>}
 */
const errorModel = Joi.object({
    error: Joi.string().example("Message d'erreur...")
}).label("Erreur");

/**
 * Modèle JOI représentant une bière.
 * @type {Joi.ObjectSchema<any>}
 */
const beerModel = Joi.object({
    id: Joi.number().integer().positive().description("L'identifiant unique de la bière").example(1),
    name: Joi.string().allow("").description("Le nom de la bière").example("Bière 1"),
    breweryId: Joi.number().integer().description("L'identifiant de la brasserie dans laquelle est la bière").example(1),
    alcohol: Joi.number().description("Le volume d'alcool dans le litre de la bière").example(5.0)
}).label("Bière")

/**
 * Modèle JOI représentant un tableau de bière.
 * @type {Joi.ArraySchema}
 */
const beersModel = Joi.array().items(beerModel.optional()).label("Bières")

/**
 * Modèle JOI représentant un token.
 * @type {Joi.ObjectSchema<any>}
 */
const token = Joi.object({
    token: Joi.string().description("Token de l'API").example("0123456789.987654321.abc")
}).label("Token")


module.exports = {
    breweryModel: breweryModel,
    breweriesModel: breweriesModel,
    beerModel: beerModel,
    beersModel: beersModel,
    errorModel: errorModel,
    token: token
};