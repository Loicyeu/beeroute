const Breweries = require("./Breweries");
const Beers = require("./Beers");
const DataTypes = require("sequelize");
const sequelize = require("../Sequelize");
const fs = require("fs");
const csv = require('fast-csv');

/**
 * Méthode permettant de créer la base de données.
 * @return {Promise<void>}
 */
function createDatabase() {
    return new Promise(async resolve => {
        Breweries.BreweriesModel = await sequelize.define("breweries", {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            city: {
                type: DataTypes.STRING,
                allowNull: false
            }
        });
        Beers.BeerModel = await sequelize.define("beers", {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            alcohol: {
                type: DataTypes.FLOAT,
                allowNull: false
            }
        });

        await Beers.BeerModel.belongsTo(Breweries.BreweriesModel, {as: "brewery"});
        Breweries.BreweriesModel.hasMany(Beers.BeerModel)

        await Breweries.BreweriesModel.sync({force: true});
        await Beers.BeerModel.sync({force: true});
        resolve()
    });
}

/**
 * Méthode permettant de populer la base de données.
 * @return {Promise<void|Object>}
 */
function populateDatabase() {
    return new Promise((resolve, reject) => {
        populateBreweries()
            .then(() => populateBeers()
                .then(() => resolve())
            )
            .catch(reason => reject(reason));
    });
}

/**
 * Méthode permettant de populer les brasseries.
 * @return {Promise<void|Object>}
 */
function populateBreweries() {
    return new Promise((resolve, reject) => {
        const fileName = './assets/open-beer-database-breweries.csv';
        fs.createReadStream(fileName, {encoding: 'utf8'})
            .pipe(csv.parse({headers: true, delimiter: ';'}))
            .on('error', error => reject(error))
            .on('data', async row => {
                try {
                    await Breweries.BreweriesModel.create({
                        id: row.id, name: row.breweries, city: row.city
                    });
                } catch (error) {
                    // Volontairement ignoré
                    // console.error('Unable to connect to insert :', row, error);
                }
            })
            .on('end', () => resolve())
            .on("close", () => resolve());
    });
}

/**
 * Méthode permettant de populer les bières.
 * @return {Promise<void|Object>}
 */
function populateBeers() {
    return new Promise((resolve, reject) => {
        const fileName = './assets/open-beer-database.csv';
        fs.createReadStream(fileName, {encoding: 'utf8'})
            .pipe(csv.parse({headers: true, delimiter: ';'}))
            .on('error', error => reject(error))
            .on('data', async row => {
                try {
                    await Beers.BeerModel.create({
                        id: row.id, name: row.name, breweryId: row.brewery_id, alcohol: row["Alcohol By Volume"]
                    });
                } catch (error) {
                    // Volontairement ignoré
                    // console.error('Unable to connect to insert :', row, error);
                }
            })
            .on('end', () => resolve())
            .on("close", () => resolve());
    });
}

/**
 * Les jeux de test des brasseries.
 * @type {({city: string, name: string, id: number}|{city: string, name: string, id: number}|{city: string, name: string, id: number}|{city: string, name: string, id: number})[]}
 */
const breweriesTest = [
    {id: 812, name: 'Magic Hat', city: 'South Burlington'},
    {id: 670, name: 'Hook Norton Brewery', city: 'Hook Norton'},
    {id: 1142, name: 'Sierra Nevada Brewing Co.', city: 'Chico'},
    {id: 779, name: 'Lighthouse Brewing', city: 'Victoria'}
];
/**
 * Les jeu de test des bières.
 * @type {({alcohol: number, breweryId: number, name: string, id: number}|{alcohol: number, breweryId: number, name: string, id: number}|{alcohol: number, breweryId: number, name: string, id: number}|{alcohol: number, breweryId: number, name: string, id: number})[]}
 */
const beersTest = [
    {id: 1, name: 'Hocus Pocus', breweryId: 812, alcohol: 4.5},
    {id: 66, name: 'Twelve Days', breweryId: 670, alcohol: 5.5},
    {id: 70, name: 'Harvest Ale 2007', breweryId: 1142, alcohol: 6.6999998093},
    {id: 73, name: 'Left Hand Brewing Company', breweryId: 670, alcohol: 8.6000003815}
]

/**
 * Méthode permettant de populer la base de données lors des tests.
 * @return {Promise<unknown>}
 */
function populateTestDatabase() {
    return new Promise((resolve, reject) => {
        Breweries.BreweriesModel.bulkCreate(breweriesTest)
            .then(() => Beers.BeerModel.bulkCreate(beersTest)
                .then(() => resolve())
                .catch(() => reject()))
            .catch(() => reject());
    });
}

module.exports = {
    createDatabase,
    populateDatabase,
    populateTestDatabase,
}