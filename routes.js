const Joi = require('joi');
const {breweryModel, breweriesModel, beersModel, beerModel, errorModel, token} = require("./models/JoiModel");
const Breweries = require("./models/Breweries");
const Beers = require("./models/Beers");
const jwt = require("jsonwebtoken");
const {BASE_URL} = require('./config');

module.exports = [
    {
        method: 'GET',
        path: BASE_URL + 'brasserie',
        options: {
            auth: false,
            handler: (request, h) => {
                return Breweries.get(request.query)
                    .then(value => h.response(value).code(200))
                    .catch(reason => h.response(reason).code(404))
            },
            description: 'Obtenir toutes les brasseries ou les brasseries correspondantes au filtrage souhaité',
            notes: "Renvoie un tableau contenant toutes les brasseries correspondantes au filtrage.",
            tags: ['api', 'Brasserie'],
            validate: {
                query: Joi.object({
                    name: Joi.string().optional().description("Le nom de la brasserie"),
                    city: Joi.string().optional().description("La ville de la brasserie")
                }),
            },
            response: {
                status: {
                    200: breweriesModel,
                    404: errorModel,
                },
            }
        }
    },
    {
        method: 'GET',
        path: BASE_URL + 'brasserie/{id}',
        options: {
            auth: false,
            handler: (request, h) => {
                return Breweries.getById(request.params.id)
                    .then(value => h.response(value).code(200))
                    .catch(reason => h.response(reason).code(404));
            },
            description: 'Obtenir une brasserie à partir de son id',
            notes: "Renvoie la brasserie ayant l'id passé dans l'URL. Dans le cas où il n'y aurait pas de brasserie, renvoie un message d'erreur.",
            tags: ['api', 'Brasserie'],
            validate: {
                params: Joi.object({
                    id: Joi.number()
                        .required()
                        .description("l'id de la brasserie souhaité."),
                })
            },
            response: {
                status: {
                    200: breweryModel,
                    404: errorModel
                },
            }
        }
    },
    {
        method: 'GET',
        path: BASE_URL + 'brasserie/random',
        options: {
            auth: false,
            handler: (request, h) => {
                const queryNumber = request.query.number;
                const number = queryNumber < 1 ? 1 : (queryNumber > 500 ? 500 : queryNumber);
                return Breweries.getRandom(number)
                    .then(value => h.response(value).code(200))
                    .catch(reason => h.response(reason).code(404));
            },
            description: 'Obtenir des brasseries aléatoires',
            notes: "Renvoie un nombre souhaité de brasserie aléatoire. Si aucune indication du nombre de brasseries à renvoyer en donne une. " +
                "Dans le cas où il n'y aurait pas de brasserie, renvoie un message d'erreur.",
            tags: ['api', 'Brasserie'],
            validate: {
                query: Joi.object({
                    number: Joi.number().integer().default(1).optional()
                        .description("Le nombre de brasseries aléatoires souhaités."),
                }),
            },
            response: {
                status: {
                    200: breweriesModel,
                    404: errorModel
                },
            }
        }
    },
    {
        method: 'POST',
        path: BASE_URL + 'brasserie',
        options: {
            auth: false,
            handler: (request, h) => {

                const payload = request.payload;

                if (payload === null) {
                    return h.response({error: "Requête mal formulée."}).code(203);
                }

                const payloadKeys = Object.keys(payload)
                if (!payloadKeys.includes("id") || !payloadKeys.includes("name") || !payloadKeys.includes("city")) {
                    return h.response({error: "Requête mal formulée."}).code(203);
                }

                return Breweries.add({id: payload.id, name: payload.name, city: payload.city})
                    .then(value => h.response(value).code(201))
                    .catch(reason => h.response(reason).code(203));
            },
            description: 'Ajouter une brasserie',
            notes: "Si la brasserie n'existe pas déjà, l'ajoute et renvoie la nouvelle brasserie créée, sinon renvoie une erreur.",
            tags: ['api', 'Brasserie'],
            validate: {
                payload: breweryModel
            },
            response: {
                status: {
                    201: breweryModel,
                    203: errorModel
                },
            }
        }

    },
    {
        method: 'PUT',
        path: BASE_URL + 'brasserie/{id}',
        options: {
            auth: false,
            handler: (request, h) => {
                const payload = request.payload;
                const id = request.params.id;

                if (payload === null) {
                    return h.response({error: "Requête mal formulée."}).code(203);
                }

                const payloadKeys = Object.keys(payload)
                if (!payloadKeys.includes("id") || payload.id !== id) {
                    return h.response({error: "Requête mal formulée."}).code(203);
                }

                return Breweries.edit(payload)
                    .then(value => h.response(value).code(201))
                    .catch(reason => h.response(reason).code(203));
            },
            description: 'Modifier une brasserie à partir de son id',
            notes: "Si la brasserie existe bien et que le payload est correct, modifie la brasserie et " +
                "renvoie la nouvelle version de la brasserie, sinon renvoie une erreur.",
            tags: ['api', 'Brasserie'],
            validate: {
                payload: Joi.object({
                    id: Joi.number().integer().required().description("L'id de la brasserie a modifier"),
                    name: Joi.string().optional().description("Le nouveau nom de la brasserie"),
                    city: Joi.string().optional().description("La nouvelle ville de la brasserie")
                }).label("Brasserie modif"),
                params: Joi.object({
                    id: Joi.number()
                        .required()
                        .description("l'id de la brasserie souhaité."),
                }),
            },
            response: {
                status: {
                    201: breweryModel,
                    203: errorModel
                },
            }
        }

    },
    {
        method: 'DELETE',
        path: BASE_URL + 'brasserie/{id}',
        options: {
            auth: 'jwt',
            handler: (request, h) => {
                const id = request.params.id;

                return Breweries.delete(id, request.query.force)
                    .then(value => h.response(value).code(201))
                    .catch(reason => h.response(reason).code(203))
            },
            description: 'Supprimer une brasserie à partir de son id',
            notes: "Si la brasserie existe bien, la supprime et revoie la brasserie supprimer, sinon renvoie une erreur.",
            tags: ['api', 'Brasserie'],
            validate: {
                params: Joi.object({
                    id: Joi.number()
                        .required()
                        .description("l'id de la brasserie a supprimer."),
                }),
                query: Joi.object({
                    force: Joi.boolean().optional().default(false).description("Forcer la suppression")
                })
            },
            response: {
                status: {
                    201: breweryModel,
                    203: errorModel
                },
            }
        }

    },

    {
        method: 'GET',
        path: BASE_URL + 'biere',
        options: {
            auth: false,
            handler: (request, h) => {
                return (async () => {
                    return Beers.get(request.query)
                        .then(value => h.response(value).code(200))
                        .catch(reason => h.response(reason).code(404));
                })();
            },
            description: "Obtenir toutes les bières de la base de données. Filtrage possible.",
            notes: "Renvoie un tableau contenant toutes les bières. Il est possible de filtrer les résultats en ajoutant des paramètres à l'URL.",
            tags: ['api', 'Bière'],
            validate: {
                query: Joi.object({
                    name: Joi.string().optional().description("Les nom de bières recherchés (séparé par des virgules)"),
                    alcohol: Joi.string().optional().description("Les taux d'alcool des bières (séparé par des virgules)"),
                    breweryId: Joi.string().optional().description("Les ids des brasseries des bières (séparé par des virgules)"),
                }),
            },
            response: {
                status: {
                    200: beersModel,
                    404: errorModel,
                },
            }
        }
    },
    {
        method: 'GET',
        path: BASE_URL + 'biere/{id}',
        options: {
            auth: false,
            handler: (request, h) => {
                return Beers.getById(request.params.id)
                    .then(value => h.response(value).code(200))
                    .catch(reason => h.response(reason).code(404));
            },
            description: 'Obtenir une bière à partir de son id',
            notes: "Renvoie la bière ayant l'id passé dans l'URL. Dans le cas où il n'y aurait pas de bière correspondante, renvoie un message d'erreur.",
            tags: ['api', 'Bière'],
            validate: {
                params: Joi.object({
                    id: Joi.number().integer().positive()
                        .required().description("l'id de la bière souhaité."),
                })
            },
            response: {
                status: {
                    200: beerModel,
                    404: errorModel
                },
            }
        }
    },
    {
        method: 'GET',
        path: BASE_URL + 'biere/random',
        options: {
            auth: false,
            handler: (request, h) => {
                const queryNumber = request.query.number;
                const number = queryNumber < 1 ? 1 : (queryNumber > 500 ? 500 : queryNumber);
                return Beers.getRandom(number)
                    .then(value => h.response(value).code(200))
                    .catch(reason => {
                        console.log(reason)
                        return h.response(reason).code(404)
                    });
            },
            description: 'Obtenir des bières aléatoires',
            notes: "Renvoie un nombre souhaité de bière aléatoire. Si aucune indication du nombre de bières à renvoyer en donne une. " +
                "Dans le cas où il n'y aurait pas de bière, renvoie un message d'erreur.",
            tags: ['api', 'Bière'],
            validate: {
                query: Joi.object({
                    number: Joi.number().integer().default(1).optional()
                        .description("Le nombre de bières aléatoires souhaités."),
                }),
            },
            response: {
                status: {
                    200: beersModel,
                    404: errorModel
                },
            }
        }
    },
    {
        method: 'POST',
        path: BASE_URL + 'biere',
        options: {
            auth: false,
            handler: (request, h) => {

                const payload = request.payload;

                if (payload === null) {
                    return h.response({error: "Requête mal formulée."}).code(203);
                }

                const payloadKeys = Object.keys(payload)
                if (!payloadKeys.includes("id") || !payloadKeys.includes("name")
                    || !payloadKeys.includes("alcohol") || !payloadKeys.includes("breweryId")) {
                    return h.response({error: "Requête mal formulée."}).code(203);
                }

                return Beers.add({
                    id: payload.id, name: payload.name, alcohol: payload.alcohol, breweryId: payload.breweryId
                })
                    .then(value => h.response(value).code(201))
                    .catch(reason => h.response(reason).code(203));
            },
            description: 'Ajouter une bière',
            notes: "Si la bière n'existe pas déjà, l'ajoute et renvoie la nouvelle bière créée, sinon renvoie une erreur.",
            tags: ['api', 'Bière'],
            validate: {
                payload: beerModel
            },
            response: {
                status: {
                    201: beerModel,
                    203: errorModel
                },
            }
        }

    },
    {
        method: 'PUT',
        path: BASE_URL + 'biere/{id}',
        options: {
            auth: false,
            handler: (request, h) => {
                const payload = request.payload;
                const id = request.params.id;

                if (payload === null) {
                    return h.response({error: "Requête mal formulée."}).code(203);
                }

                const payloadKeys = Object.keys(payload)
                if (!payloadKeys.includes("id") || payload.id !== id) {
                    return h.response({error: "Requête mal formulée."}).code(203);
                }

                return Beers.edit(payload)
                    .then(value => h.response(value).code(201))
                    .catch(reason => h.response(reason).code(203));
            },
            description: 'Modifier une bière à partir de son id',
            notes: "Si la bière existe bien et que le payload est correct, modifie la bière et " +
                "renvoie la nouvelle version de la bière, sinon renvoie une erreur.",
            tags: ['api', 'Bière'],
            validate: {
                payload: Joi.object({
                    id: Joi.number().integer().required().description("L'id de la bière a modifier"),
                    name: Joi.string().optional().description("Le nouveau nom de la bière"),
                    alcohol: Joi.number().optional().description("Le taux d'alcool de la bière"),
                    breweryId: Joi.number().integer().optional().description("La brasserie de la bière")
                }).label("Bière modif"),
                params: Joi.object({
                    id: Joi.number()
                        .required()
                        .description("l'id de la bière souhaité."),
                }),
            },
            response: {
                status: {
                    201: beerModel,
                    203: errorModel
                },
            }
        }

    },
    {
        method: 'DELETE',
        path: BASE_URL + 'biere/{id}',
        options: {
            auth: 'jwt',
            handler: (request, h) => {
                const id = request.params.id;

                return Beers.delete(id)
                    .then(value => h.response(value).code(201))
                    .catch(reason => h.response(reason).code(203))
            },
            description: 'Supprimer une bière à partir de son id',
            notes: "Si la bière existe bien, la supprime et revoie la bière supprimer, sinon renvoie une erreur.",
            tags: ['api', 'Bière'],
            validate: {
                params: Joi.object({
                    id: Joi.number()
                        .required()
                        .description("l'id de la bière a supprimer."),
                }),
            },
            response: {
                status: {
                    201: beerModel,
                    203: errorModel
                },
            }
        }

    },

    {
        method: '*',
        path: '/{any*}',
        options: {
            auth: false,
            handler: (request, h) => {
                return h.response("404 Error! Page Not Found!").code(404);
            }
        }
    },
    {
        method: 'GET',
        path: BASE_URL + 'generate/{id}/{name}',
        options: {
            auth: false,
            handler: (request, h) => {
                let token = jwt.sign({id: request.params.id, name: request.params.name}, 'L3_R3MM_L3_B3ST');
                return h.response({token: token}).code(200)
            },
            description: 'Générer un token',
            notes: "Permet de générer un token permettant de supprimer des bières et des brasseries dans la base de données. " +
                "Le token n'est valide que si les identifiants données sont dans la liste des personnes valide.",
            tags: ['api', 'token'],
            validate: {
                params: Joi.object({
                    id: Joi.number()
                        .required()
                        .description("L'id de l'utilisateur."),
                    name: Joi.string()
                        .required()
                        .description("Le nom de l'utilisateur")
                }),
            },
            response: {
                status: {
                    200: token
                },
            }
        }
    },
    {
        method: 'GET',
        path: BASE_URL + 'tokenTest',
        options: {
            auth: 'jwt',
            handler: (request, h) => {
                return {valid: true};
            },
            description: 'test route auth',
            notes: "Permet de tester votre token pour savoir si celui est valide, cet a dire qu'il fait parti des token provenant de personnes valides.",
            tags: ['api', 'token'],
            response: {
                status: {
                    200: Joi.object({
                        valid: Joi.boolean().example(true)
                    })
                },
            }
        }
    }]