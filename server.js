"use strict";

const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const routes = require("./routes");
const {createDatabase, populateDatabase, populateTestDatabase} = require("./models/database");
const AuthJwt = require('hapi-auth-jwt2');
const {HOST, PORT} = require('./config');

const users = {
    1337: {id: 1337, name: 'Remm'},
    7331: {id: 7331, name: 'Henry'},
    24: {id: 24, name: 'Cabon'},
    0: {id: 0, name: 'test'}
}

const validate = async function (decoded, request, h) {
    if (users[decoded.id]) {
        return {isValid: true};
    } else {
        return {isValid: false};
    }
}

exports.init = async () => {
    await createDatabase();
    await populateTestDatabase();

    const server = Hapi.server({
        port: PORT,
        host: HOST,
    });

    const swaggerOptions = {
        info: {
            title: "Documentation de l'API Breeroute",
            version: Pack.version,
        },
    };

    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);

    await server.register({
        plugin: AuthJwt
    });

    server.auth.strategy('jwt', 'jwt', {
        key: 'L3_R3MM_L3_B3ST',
        validate
    });

    server.route(routes);

    await server.initialize();
    return server;
};

exports.start = async () => {
    await createDatabase();
    await populateDatabase();

    const server = Hapi.server({
        port: PORT,
        host: HOST,
    });

    const swaggerOptions = {
        info: {
            title: "Documentation de l'API Breeroute",
            version: Pack.version,
        },
        grouping: 'tags'
    };

    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);

    await server.register({
        plugin: AuthJwt
    });

    server.auth.strategy('jwt', 'jwt', {
        key: 'L3_R3MM_L3_B3ST',
        validate
    });

    server.route(routes);

    await server.start();
    console.log('Server running on %s', server.info.uri);
    return server;
}

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});