"use strict";

const Lab = require('@hapi/lab');
const {expect} = require('@hapi/code');
const {afterEach, beforeEach, describe, it} = exports.lab = Lab.script();
const {init} = require('../server');

const testKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MCwibmFtZSI6InRlc3QiLCJpYXQiOjE2MTY3NTc4ODd9.DY4hT0ogQKM8egbHToxedbV3xmlkpcQoVIe9wHYAv_Q'

/*
ROUTE : GET
/api/v1/biere?    Renvoie toutes les bières
     params:
         brasserieId
         alcool
         nom

/api/v1/biere/{id}   Renvoie la bière ayant cet id
/api/v1/biere/random    Renvoie une bière random

ROUTE : POST
/api/v1/biere
    payload:
        id
        name
        brasserie
        ...

ROUTE : PUT
/api/v1/biere/{id}
    payload:
        name
        brasserie
        ...

ROUTE : DELETE
/api/v1/biere/{id}  Delete une bière
*/

describe('GET -', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    //region test-get-brasseries
    it('/api/v1/brasserie  responds with model', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie'
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.once.include([
            {id: 812, name: 'Magic Hat', city: 'South Burlington'},
            {id: 670, name: 'Hook Norton Brewery', city: 'Hook Norton'},
            {id: 1142, name: 'Sierra Nevada Brewing Co.', city: 'Chico'},
            {id: 779, name: 'Lighthouse Brewing', city: 'Victoria'}
        ]);
    });

    it('/api/v1/brasserie/{id}  responds with model', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/779'
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.equal(
            {id: 779, name: 'Lighthouse Brewing', city: 'Victoria'}
        );
    });

    it('/api/v1/brasserie/{id}  responds with error', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/7331'
        });
        expect(res.statusCode).to.equal(404);
        expect(res.result).to.equal(
            {error: "La brasserie n'a pas pu être trouvé."}
        );
    });

    it('/api/v1/brasserie/random  - 2 aléatoire', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/random?number=2'
        })
        expect(res.statusCode).to.equal(200)
        expect(res.result.length).to.equal(2)
    })

    it('/api/v1/brasserie/random  - 10 aléatoires', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/random?number=10'
        })
        expect(res.statusCode).to.equal(200)
        expect(res.result.length).to.equal(4)
    })

    it('/api/v1/brasserie/random  - -10 aléatoires', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/random?number=-10'
        })
        expect(res.statusCode).to.equal(200)
        expect(res.result.length).to.equal(1)
    })

    it('/api/v1/brasserie?name=yomeccvbienoubien  responds with error', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie?name=yomeccvbienoubien'
        })
        expect(res.statusCode).to.equal(404)
        expect({error: "Aucune brasserie n'a été trouvé."}
        ).to.equal(res.result)
    });

    it('/api/v1/brasserie?jeSaisPas=MoiNonPlus  responds with error', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie?jeSaisPas=MoiNonPlus'
        })
        expect(res.statusCode).to.equal(400)
    });
    //endregion

    //region test-get-bieres
    it('/api/v1/biere  responds with model', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/biere'
        })
        expect(res.statusCode).to.equal(200)
        expect(
            [
                {id: 1, name: 'Hocus Pocus', breweryId: 812, alcohol: 4.5},
                {id: 66, name: 'Twelve Days', breweryId: 670, alcohol: 5.5},
                {id: 70, name: 'Harvest Ale 2007', breweryId: 1142, alcohol: 6.6999998093},
                {id: 73, name: 'Left Hand Brewing Company', breweryId: 670, alcohol: 8.6000003815}
            ]
        ).to.once.include(res.result)
    })

    it('/api/v1/biere?breweryId=670&alcohol=5.5  responds with model', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/biere?breweryId=670&alcohol=5.5'
        })
        expect(res.statusCode).to.equal(200)
        expect(
            [
                {id: 66, name: 'Twelve Days', breweryId: 670, alcohol: 5.5},
            ]
        ).to.once.include(res.result)
    })

    it('/api/v1/biere?name=Hocus%20Pocus,Twelve%20Days  responds with model', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/biere?name=Hocus%20Pocus,Twelve%20Days'
        });
        expect(res.statusCode).to.equal(200)
        expect(
            [
                {id: 1, name: 'Hocus Pocus', breweryId: 812, alcohol: 4.5},
                {id: 66, name: 'Twelve Days', breweryId: 670, alcohol: 5.5},
            ]
        ).to.once.include(res.result)
    })

    it('/api/v1/biere/{id}  responds with model', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/biere/73'
        })
        expect(res.statusCode).to.equal(200)
        expect(
            [
                {id: 73, name: 'Left Hand Brewing Company', breweryId: 670, alcohol: 8.6000003815}
            ]
        ).to.once.include(res.result)
    })

    it('/api/v1/biere/random  - 2 aléatoire', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/biere/random?number=2'
        })
        expect(res.statusCode).to.equal(200)
        expect(res.result.length).to.equal(2)
    })

    it('/api/v1/biere/random  - 10 aléatoires', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/biere/random?number=10'
        })
        expect(res.statusCode).to.equal(200)
        expect(res.result.length).to.equal(4)
    })

    it('/api/v1/biere/random  - -10 aléatoires', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/biere/random?number=-10'
        })
        expect(res.statusCode).to.equal(200)
        expect(res.result.length).to.equal(1)
    })

    it('/api/v1/biere/{id}  responds with error', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/biere/1337'
        });
        expect(res.statusCode).to.equal(404)
        expect({error: "La bière n'a pas pu être trouvé."})
            .to.equal(res.result)
    })

    it('/api/v1/biere?name=yomeccvbienoubien  responds with error', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/biere?name=yomeccvbienoubien'
        });
        expect(res.statusCode).to.equal(404)
        expect({error: "Aucune bière n'a été trouvé."})
            .to.equal(res.result)
    })

    it('/api/v1/brasserie?jeSaisPas=MoiNonPlus  responds with error', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie?jeSaisPas=MoiNonPlus'
        });
        expect(res.statusCode).to.equal(400)
    });
    //endregion

    it('URL non gérée /', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/'
        });
        expect(res.statusCode).to.equal(404);
        expect(res.result).to.equal('404 Error! Page Not Found!');
    });
});

describe('POST -', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    //region test-post-brasseries
    it("Ajout correct {id: '1950', name: 'remmBrew', city: 'remmland'} - brasserie ", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/brasserie',
            payload: {id: 1950, name: "remmBrew", city: "remmland"}
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({id: 1950, name: 'remmBrew', city: 'remmland'})
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/1950'
        });
        expect(res_.statusCode).to.equal(200);
        expect(res_.result).to.equal({id: 1950, name: 'remmBrew', city: 'remmland'});
    });

    it("Ajout existant {id: 812, name: 'Magic Hat', city: 'South Burlington'} - brasserie", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/brasserie',
            payload: {id: 812, name: "Magic Hat", city: "South Burlington"}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "La brasserie existe déjà."})
    });

    it("Ajout existant {id: 812, name: 'remmBrew', city: 'remmland'} - brasserie", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/brasserie',
            payload: {id: 812, name: "remmBrew", city: "remmland"}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "La brasserie existe déjà."})
    });

    it("Ajout sans payload - brasserie", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/brasserie',
        });
        expect(res.statusCode).to.equal(400);
    });

    it("Ajout avec mauvais payload - brasserie", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/brasserie',
            payload: {}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "Requête mal formulée."})
    });
    //endregion

    //region test-post-bieres
    it("Ajout correct {id: 812, name: 'remmBeer', breweryId: 812, alcohol: 5.0} - bière", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/biere',
            payload: {id: 812, name: "remmBeer", breweryId: 812, alcohol: 5.0}
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({id: 812, name: 'remmBeer', breweryId: 812, alcohol: 5.0})
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/biere/812'
        });
        expect(res_.statusCode).to.equal(200);
        expect(res_.result).to.equal({id: 812, name: 'remmBeer', breweryId: 812, alcohol: 5.0});
    });

    it("Ajout existant {id: 1, name: 'Hocus Pocus', breweryId: 812, alcohol:'4.5'} - bière", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/biere',
            payload: {id: 1, name: "Hocus Pocus", breweryId: 812, alcohol: 4.5}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "La bière existe déjà ou ne peut être ajouté."})
    });

    it("Ajout existant {id: 1, name: 'remmBeer', breweryId: 812, alcohol:90} - bière", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/biere',
            payload: {id: 1, name: "remmBeer", breweryId: 812, alcohol: 90}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "La bière existe déjà ou ne peut être ajouté."})
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/biere/1'
        })
        expect(res_.statusCode).to.equal(200);
        expect(res_.result).to.equal({id: 1, name: 'Hocus Pocus', breweryId: 812, alcohol: 4.5});
    });

    it("Ajout avec mauvais breweryId {id: 96, name: 'remmBeer', breweryId: 1111, alcohol:90} - bière", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/biere',
            payload: {id: 96, name: "remmBeer", breweryId: 1111, alcohol: 90}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "La bière existe déjà ou ne peut être ajouté."})
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/biere/96'
        })
        expect(res_.statusCode).to.equal(404);
        expect(res_.result).to.equal({error: "La bière n'a pas pu être trouvé."});
    });

    it("Ajout sans payload - bière", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/biere',
        });
        expect(res.statusCode).to.equal(400);
    });

    it("Ajout avec mauvais payload - bière", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/biere',
            payload: {}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "Requête mal formulée."})
    });
    //endregion

    it('URL non gérée /', async () => {
        const res = await server.inject({
            method: 'post',
            url: '/'
        });
        expect(res.statusCode).to.equal(404);
        expect(res.result).to.equal('404 Error! Page Not Found!');
    });


});

describe('PUT -', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    //region test-put-brasserie
    it('Modification nom  - brasserie ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/brasserie/812',
            payload: {id: 812, name: 'RemmBrew'}
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({id: 812, name: 'RemmBrew', city: 'South Burlington'});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/812'
        });
        expect(res_.statusCode).to.equal(200)
        expect(res_.result).to.equal({id: 812, name: 'RemmBrew', city: 'South Burlington'})
    });

    it('Modification nom, ville  - brasserie ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/brasserie/1142',
            payload: {id: 1142, name: 'RemmBrew', city: 'RemmLand'}
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({id: 1142, name: 'RemmBrew', city: 'RemmLand'});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/1142'
        });
        expect(res_.statusCode).to.equal(200);
        expect(res_.result).to.equal({id: 1142, name: 'RemmBrew', city: 'RemmLand'});
    });

    it('Modification identique  - brasserie ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/brasserie/812',
            payload: {id: 812, name: 'Hat Magic', city: 'South Burlington'}
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({id: 812, name: 'Hat Magic', city: 'South Burlington'});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/812'
        })
        expect(res_.statusCode).to.equal(200);
        expect(res_.result).to.equal({id: 812, name: 'Hat Magic', city: 'South Burlington'});
    });

    it('Modification sur element inexistant  - brasserie', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/brasserie/7331',
            payload: {id: 7331, name: 'Magic Hat', city: 'South Burlington'}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "La brasserie n'a pas pu être modifiée."});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/7331'
        });
        expect(res_.statusCode).to.equal(404)
        expect(res_.result).to.equal({error: "La brasserie n'a pas pu être trouvé."})
    });

    it('Modification avec id différents  - brasserie ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/brasserie/812',
            payload: {id: 1142, name: 'RemmLand'}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "Requête mal formulée."});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/812'
        });
        expect(res_.statusCode).to.equal(200);
        expect(res_.result).to.equal({id: 812, name: 'Magic Hat', city: 'South Burlington'});
    });

    it('Modification sans payload  - brasserie', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/brasserie/812'
        });
        expect(res.statusCode).to.equal(400);
    });

    it('Modification avec un payload non conforme  - brasserie ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/brasserie/812',
            payload: {chat: 44}
        });
        expect(res.statusCode).to.equal(400);
    });

    //endregion

    //region test-put-bieres
    it('Modification nom  - bière', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/1',
            payload: {id: 1, name: 'RemmBeer', breweryId: 812, alcohol: 4.5}
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({id: 1, name: 'RemmBeer', breweryId: 812, alcohol: 4.5});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/biere/1'
        });
        expect(res_.statusCode).to.equal(200)
        expect(res_.result).to.equal({id: 1, name: 'RemmBeer', breweryId: 812, alcohol: 4.5});
    });

    it('Modification nom, alcool  - bière', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/66',
            payload: {id: 66, name: 'RemmBeer', breweryId: 812, alcohol: 90}
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({id: 66, name: 'RemmBeer', breweryId: 812, alcohol: 90});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/biere/66'
        })
        expect(res_.statusCode).to.equal(200);
        expect(res_.result).to.equal({id: 66, name: 'RemmBeer', breweryId: 812, alcohol: 90});
    });

    it('Modification nom, alcool, brasserie  - bière', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/73',
            payload: {id: 73, name: 'RemmBeer', breweryId: 1142, alcohol: 90}
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({id: 73, name: 'RemmBeer', breweryId: 1142, alcohol: 90});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/biere/73'
        })
        expect(res_.statusCode).to.equal(200);
        expect(res_.result).to.equal({id: 73, name: 'RemmBeer', breweryId: 1142, alcohol: 90});
    });

    it('Modification identique  - bière ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/70',
            payload: {id: 70, name: 'Harvest Ale 2007', breweryId: 1142, alcohol: 6.6999998093}
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({id: 70, name: 'Harvest Ale 2007', breweryId: 1142, alcohol: 6.6999998093});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/biere/70'
        })
        expect(res_.statusCode).to.equal(200);
        expect(res_.result).to.equal({id: 70, name: 'Harvest Ale 2007', breweryId: 1142, alcohol: 6.6999998093});
    });

    it('Modification sur element inexistant  - bière', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/7331',
            payload: {id: 7331, name: 'Pocus Hocus', breweryId: 812, alcohol: 4.5}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "La bière n'a pas pu être modifiée."});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/biere/7331'
        })
        expect(res_.statusCode).to.equal(404);
        expect(res_.result).to.equal({error: "La bière n'a pas pu être trouvé."});
    });

    it('Modification avec mauvais breweryId  - bière', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/1',
            payload: {id: 1, name: 'Pocus Hocus', breweryId: 42, alcohol: 4.5}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "La bière n'a pas pu être modifiée."});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/biere/1'
        });
        expect(res_.statusCode).to.equal(200);
        expect(res_.result).to.equal({id: 1, name: 'Hocus Pocus', breweryId: 812, alcohol: 4.5});
    });

    it('Modification avec id différents  - bière', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/73',
            payload: {id: 1, name: 'Pocus Hocus', breweryId: 812, alcohol: 4.5}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "Requête mal formulée."});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/biere/73'
        });
        expect(res_.statusCode).to.equal(200);
        expect(res_.result).to.equal({id: 73, name: 'Left Hand Brewing Company', breweryId: 670, alcohol: 8.6000003815}
        )
    });

    it('Modification sans payload  - bière', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/1'
        });
        expect(res.statusCode).to.equal(400);
    });

    it('Modification avec un payload non conforme  - bière ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/70',
            payload: {chat: 44}
        });
        expect(res.statusCode).to.equal(400);
    });

    //endregion

    it('URL non gérée /', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/'
        });
        expect(res.statusCode).to.equal(404);
        expect(res.result).to.equal('404 Error! Page Not Found!');
    });
});

describe('DELETE -', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    //region test-delete-brasseries
    it('Suppression correcte - brasserie', async () => {
        const res = await server.inject({
            method: 'delete',
            headers: {
                Authorization: testKey
            },
            url: '/api/v1/brasserie/779',
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({id: 779, name: 'Lighthouse Brewing', city: 'Victoria'});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/779'
        });
        expect(res_.statusCode).to.equal(404);
        expect(res_.result).to.equal({error: "La brasserie n'a pas pu être trouvé."});
    });

    it('Suppression forcé sur brasserie ayant des bières - brasserie', async () => {
        const res = await server.inject({
            method: 'delete',
            headers: {
                Authorization: testKey
            },
            url: '/api/v1/brasserie/670?force=true',
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({id: 670, name: 'Hook Norton Brewery', city: 'Hook Norton'});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/biere'
        });
        expect(res_.statusCode).to.equal(200);
        expect(res_.result).to.once.not.include([
            {id: 66, name: 'Twelve Days', breweryId: 670, alcohol: 5.5},
            {id: 73, name: 'Left Hand Brewing Company', breweryId: 670, alcohol: 8.6000003815}
        ]);
    });

    it('Suppression sur element inexistant  - brasserie', async () => {
        const res = await server.inject({
            method: 'delete',
            headers: {
                Authorization: testKey
            },
            url: '/api/v1/brasserie/7331',
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "La brasserie n'a pas pu être supprimée."});
    });

    it("Suppression sur d'une brasserie ayant des bieres - brasserie", async () => {
        const res = await server.inject({
            method: 'delete',
            headers: {
                Authorization: testKey
            },
            url: '/api/v1/brasserie/670',
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "La brasserie n'a pas pu être supprimée."});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/670'
        });
        expect(res_.statusCode).to.equal(200);
        expect(res_.result).to.equal({id: 670, name: 'Hook Norton Brewery', city: 'Hook Norton'});
    });
    //endregion

    //region test-delete-bieres
    it('Suppression correcte - bieres', async () => {
        const res = await server.inject({
            method: 'delete',
            headers: {
                Authorization: testKey
            },
            url: '/api/v1/biere/1',
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({id: 1, name: 'Hocus Pocus', breweryId: 812, alcohol: 4.5});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/biere/1'
        });
        expect(res_.statusCode).to.equal(404);
        expect(res_.result).to.equal({error: "La bière n'a pas pu être trouvé."});
    });

    it('Suppression sur un element inexistant - bieres', async () => {
        const res = await server.inject({
            method: 'delete',
            headers: {
                Authorization: testKey
            },
            url: '/api/v1/biere/7331',
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal({error: "La bière n'a pas pu être supprimée."});
        const res_ = await server.inject({
            method: 'get',
            url: '/api/v1/biere/7331'
        });
        expect(res_.statusCode).to.equal(404);
        expect(res_.result).to.equal({error: "La bière n'a pas pu être trouvé."});
    });
    //endregion

    it('URL non gérée /', async () => {
        const res = await server.inject({
            method: 'delete',
            headers: {
                Authorization: testKey
            },
            url: '/'
        });
        expect(res.statusCode).to.equal(404);
        expect(res.result).to.equal('404 Error! Page Not Found!');
    });
});

describe('TOKEN - ', () => {

    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    //region test-token
    it('token non valide', async() => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/generate/314159/pi'
        });
        expect(res.statusCode).to.equal(200);
        let token = res.result.token;
        const res_ = await server.inject({
            method: 'get',
            headers: {
                Authorization:token
            },
            url: '/api/v1/tokenTest'
        });
        expect(res_.statusCode).to.equal(401);
    })

    it('token valide', async() => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/generate/1337/Remm'
        });
        expect(res.statusCode).to.equal(200);
        let token = res.result.token;
        const res_ = await server.inject({
            method: 'get',
            headers: {
                Authorization:token
            },
            url: '/api/v1/tokenTest'
        })

    })
    //endregion
})